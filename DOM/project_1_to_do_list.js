const signUp         = document.getElementById("signUp");
const logIn          = document.getElementById("logIn");
const main           = document.getElementById("main");
const myForm         = document.getElementById("formDiv");
const loginuser      = document.getElementById("mainLoginUser");
const myTodoList     = document.getElementById("myTodoList");
const buttonSubmit   = document.getElementById("submitB");
const logInForm      = document.getElementById('loginForm');
const createTask     = document.getElementById("createTask");
const clearAll       = document.getElementById("clearAll");
const buttonEditList = document.getElementsByTagName("button");
const buttonLogOut   = document.getElementById("LogOut");
const modifyUser     = document.getElementById("modifyUser");
const elements       = document.getElementsByName("toEdit");

myForm.style.display       = "none";
logInForm.style.display    = "none";
myTodoList.style.display   = "none";

let storageUser = {};


modifyUser.addEventListener("click", function() {
   let oList      = getTodoList({email: window.globalEmail});
   //location.reload()
   myForm.style.display       = "block"
   main.style.display         = "none";
   loginuser.style.display    = "block";
   logInForm.style.display    = "none";
   myTodoList.style.display   = "none";
   document.getElementById('valueFirstName').value = oList.name;
   document.getElementById('lastNameId').value     = oList.lastName;
   document.getElementById('valueEmail').value     = oList.email;
   document.getElementById('valueEmail').readOnly  = true;
   document.getElementById('valuePassword').value  = oList.password;
});

buttonLogOut.addEventListener("click", function() {
   console.log("log out");
   window.globalEmail = '';
   console.log(window.globalEmail);
   location.reload()
   document.getElementById("newForm").reset();
});


clearAll.addEventListener("click", function(e) {
   let oList      = getTodoList({email: window.globalEmail});
   oList.todoList = {};
   localStorage.setItem(window.globalEmail, JSON.stringify(oList))
   showDashboard();
});


function getCheckboxes() {
   let count         = 0;
   const checkboxes  = document.querySelectorAll('[type="checkbox"]');
   let newCheckBox   = {};

   checkboxes.forEach((checkbox) => {
      if(checkbox.name != 'termsOfUse') {
         newCheckBox[checkbox.name] = checkbox.checked;     //add our value into our object
         const id = checkbox. id;
         count++; // dont need it
      }
   });
   let oList = getTodoList({email: window.globalEmail});
   oList.todoList = newCheckBox;
   localStorage.setItem(window.globalEmail, JSON.stringify(oList));
}

function getListToEdit() {
   const listToEdit = document.querySelectorAll
}

function validateTask(newTask, tasks) {
   let repeat = false;
   // console.log(newTask);
   // console.log(tasks);
   for (const [key, value] of Object.entries(tasks)) {
      console.log(key);
      if(key === newTask) {
         console.log('pos falso');
         repeat = true;
      }
   }
   if(repeat) {
      alert(`The value ${newTask} is already added`);
      return false;
   }
   return true
}

function addNewTask(task) {
   //const newTask = {[task]: false};
   let oList = getTodoList({email: window.globalEmail});

   const isValid = validateTask(task, oList.todoList);
   if(isValid) {
      oList.todoList[task] = false;
      console.log(oList);
      const todoList = oList.todoList;
      
      localStorage.setItem(window.globalEmail, JSON.stringify(oList))
      showDashboard(todoList);
   }
}

createTask.addEventListener("click", function(e) {
   task = window.prompt("Please introduce a task");
   addNewTask(task);
   
});

signUp.addEventListener("click", function() {   //first time
   main.style.display = "none";
   myForm.style.display="block"

});

logIn.addEventListener('click', () => {
   main.style.display = "none";
   logInForm.style.display="block"
});


function submit() {
   //const formtl = document.getElementById("form").value;
   //console.log(formtl);
}

function validateEmail(email) {
   const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   return regex.test(String(email).toLowerCase());
}

function validateData(oForm) {
   let resForm = {};
   let error   = {};

   for (const [key, value] of Object.entries(oForm)) {
      //console.log(key);
      if(key == "email") {
         const resEmail = validateEmail(value);
         if(resEmail) {
            document.getElementById("emailError").innerHTML = "";
            resForm[key]= { "key": key, "status" : true };
         } else {
            error[key]= {
               "status" : false,
               "key": key,
               "description" : "There is not a valid email"
            }
            document.getElementById("emailError").innerHTML = error[key].description;
         }
      }
      else if(key == "lastName" ) {
         if(value.length > 0) {
            document.getElementById("lastNameError").innerHTML = "";
            resForm[key]= { "key": key, "status" : true };
            
         } else {
            error[key]= {
               "status" : false,
               "element": key,
               "description" : `include ${key}`
            }
            document.getElementById("lastNameError").innerHTML = error[key].description;
         }
      }
      else if(key == "name" ) {
         if(value.length > 0) {
            document.getElementById("nameError").innerHTML = "";
            resForm[key]= { "key": key, "status" : true };
            
         } else {
            error[key]= {
               "status" : false,
               "element": key,
               "description" : `include ${key}`
            }
            document.getElementById("nameError").innerHTML = error[key].description;
         }
      }
      else if(key == "password") {
         if(value.length >= 8) {
            document.getElementById("passwordError").innerHTML = "";
            resForm[key]= { "key": key, "status" : true };
         }
         else {
            error[key]= {
               "status" : false,
               "element": key,
               "description" : "include a password with at least 8 characters"
            }
            document.getElementById("passwordError").innerHTML = error[key].description;
         }
      }
   
      else if(key == "termsOfUse") {
         if(value) {
           resForm[key]= { "key": key, "status" : true };
           document.getElementById("termsOfUseError").innerHTML = "";
         } else {
            error[key]= {
               "status" : false,
               "element": key,
               "description" : "Please accept terms of use"
            }
            document.getElementById("termsOfUseError").innerHTML = error[key].description;
         }
      }
      else { console.log("should not been seen"); }

   }
   return {error, resForm};

}

function getTodoList(email) {
   //console.log('el gettodollistemail');
   //console.log(email);
   const readLocal = localStorage.getItem(email.email);
   return JSON.parse(readLocal);

}

function showDashboard(todoList) {
   console.log('entro aqui');
   todoList = todoList ? todoList : {};
   //todoList = { soy :false, jorge : false, yorsh: true }; //test
   //todoList.sort();
   let itemsTable = document.getElementById("listToDo");
   let count = 0;
   var Table = document.getElementById("listToDo");
   Table.innerHTML = "";

   for(const [key, value] of Object.entries(todoList)) {
      const checked = value ? 'checked' : "";
      //console.log(checked);
      itemsTable.innerHTML += '<label><tr><td   ><input    type="checkbox"'+ checked+' name="'+key+'" id="task_'+ count +'" /> ' +key +' <button type="button" onclick="myFunction(this)" name="toEdit" id="edit_'+key+'">Edit</button>  </td>  </tr></label>';
      count++;

   }

   //for(let  ctr = 0; ctr < todoList.length; ctr++) {
   //   itemsTable.innerHTML += '<tr><td>' + todoList[ctr] + '</td></tr>';
   //}
}

function dashboard(email) {
   myTodoList.style.display = "block";
   loginuser.style.display = "none";
   const todoUser = getTodoList(email);

   showDashboard(todoUser.todoList);
}


function submitSigninForm(event) {
   event.preventDefault();
   let oList = getTodoList({email: window.globalEmail});


   const name     = event.target['name'].value;
   const lastName = event.target['lastName'].value;
   const email    = event.target['email'].value;
   const password = event.target['password'].value;
   const terms    = event.target['termsOfUse'].checked;
   const oForm    = {
      name      : name,
      lastName  : lastName,
      email     : email,
      password  : password,
      termsOfUse: terms,
      todoList  : oList ? oList.todoList : {},
   }
   let res = validateData(oForm);
   
   if(typeof res.error === 'object' && Object.keys(res.error).length === 0) {
      console.log("saving on localStorage");
      window.globalEmail = email;
      localStorage.setItem(email, JSON.stringify(oForm));
      dashboard(oForm);
   }
}

function validateUserLogin(user, password) {
   if(user) {
      if(user.password == password) {
         document.getElementById("emailErrorLogin").innerHTML = "";
         document.getElementById("passwordErrorLogin").innerHTML = "";
         return true;

      } else {
         document.getElementById("emailErrorLogin").innerHTML = "";
         document.getElementById("passwordErrorLogin").innerHTML = "The password is incorrect";
      }

   } else {
      document.getElementById("emailErrorLogin").innerHTML = "The email is not registered";
   }
}

function submitLoginForm(event) {
   event.preventDefault();

   const email    = event.target['email'].value;
   const password = event.target['password'].value;

   storageUser = localStorage.getItem(email);
   console.log('submitloging',storageUser);
   const isValid = validateUserLogin(JSON.parse(storageUser), password);
   if(isValid) {
      window.globalEmail = email;
      dashboard(JSON.parse(storageUser));
   }
}

function myFunction(btn) {
   let newListEdit = {};
   const parent = btn.parentNode.firstChild;
   var id = parent.getAttribute("id");


   console.log("you clicked");
   // console.log(parent);
   // console.log(parent.name);
   // console.log(id);
   newListEdit['new'] = prompt("Please modify your List", parent.name);
   newListEdit['old'] = parent.name;
   
   if(newListEdit['new'] != newListEdit['old']) {
      modifyList(newListEdit);
   }
}

function modifyList(newList) {
   const oldValue = newList.old;
   const newValue = newList.new;
   //const newTask = {[task]: false};
   let oList      = getTodoList({email: window.globalEmail}); // get all
   let tasks      = oList.todoList;                          //separate todolist
   const isValid  = validateTask(newValue, tasks);
   
   if(isValid) {
      tasks[newValue]= tasks[oldValue];                   //add new value
      delete tasks[oldValue];                              //we need to delete the old value
      oList.todoList = tasks;                              //add task to our object
      localStorage.setItem(window.globalEmail, JSON.stringify(oList))
      showDashboard(tasks);
   }

}

function goBack() {
   location.reload();
 }
