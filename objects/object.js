function Animal_example(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
}

Animal_example.prototype.sayBreed = function() {
    console.log(`I'am a ${this.breed}`);
}

const dog1 = new Animal_example('bodys', ' 15', 'french poddle'); // dog1 inherints from prototype Animal_example, whic inherits from prototype object
const dog2 = new Animal_example('frida', 4, 'fritongon');
const dog3 = new Animal_example('mulan', 2, 'chihuapug');

//dog1.sayBreed();




/********** */

function Animal(name, age) {
    this.name = name;
    this.age = age;
}

//solo puedo llammar esta funcion asignando la funcion(clase) Dog , poero si la cambio a animal, tengo que hacer lo de Object.create(Animal.prototype)
Animal.prototype.makeNoise = function() {
    console.log(`generic animal noise`);
}

Cat.prototype = Object.create(Animal.prototype);
Cat.prototype.constructor = Cat;
Cat.prototype.scratchPost = function() {
    console.log('cat has scratched the post');
}

function Dog(name, age, breed) {
    Animal.call(this, name, age);
    this.breed = breed;
}

function Cat(name, age, coloration) {
    Animal.call(this, name, age);
    this.coloration = coloration;
}

const mulonchas = new Dog('mulan', 5 , 'chihuapug');
// mulonchas.makeNoise();

const jirijiri = new Cat('jirijiri', 5, 'white');
jirijiri.scratchPost();
jirijiri.makeNoise();